import {ReactNode} from 'react'
import "../styles/globals.css";
import {SideBar} from '@/components/SideBar/SideBar'
import { NavBar } from '@/components/NavBar'
export default function RootLayout({
  children
}: {
  children: ReactNode
}) {
  return (
    <html lang="en">
      <body>
        <NavBar />
        <SideBar>
          {children}
        </SideBar>
      </body>
    </html>
  )
}
