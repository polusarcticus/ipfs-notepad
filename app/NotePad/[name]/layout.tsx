'use client'

import { useState, useEffect } from 'react'
import { usePathname, useRouter  } from 'next/navigation';
import { useNotePad } from '@/hooks/useNotePads'
import { NewNoteButton } from '@/components/NewNoteButton'
import { NotePadContext } from '@/context/NotePadsContext'

export default function Layout({children}) {
  const pathname = usePathname()
  const splitPath = pathname.split('/')
  const name = splitPath[2]
  const router = useRouter()
  const { notes, fetchNotePad } = useNotePad({defaultName:name})
  const [noteButtons, setNoteButtons] =useState([])
  useEffect(() => {
    if (notes.length) {
    setNoteButtons(
      //@ts-ignore
      notes.map((note,i) => {
        console.log('note', note)
        return (
          <button
            onClick={() => {
              router.push(`${pathname}/note/${note.name}`)
            }}
            key={i} className="flex items-center w-full px-5 py-2 transition-colors duration-200 bg-gray-100 dark:bg-gray-800 gap-x-2 focus:outline-none">
            <div className="relative">
              <img className="object-cover w-8 h-8 rounded-full" src="https://images.unsplash.com/photo-1531746020798-e6953c6e8e04?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=764&h=764&q=100" alt=""/>
              <span className="h-2 w-2 rounded-full bg-emerald-500 absolute right-0.5 ring-1 ring-white bottom-0"></span>
            </div>

            <div className="text-left rtl:text-right">
              <h1 className="text-sm font-medium text-gray-700 capitalize dark:text-white">{decodeURI(note.name)}</h1>

              <p className="text-xs text-gray-500 dark:text-gray-400">{note.date.toString()}</p>
            </div>
          </button>
        )
      })
    )
    }
  }, [notes])
  return (<NotePadContext.Provider value={{notes, fetchNotePad}}>
      <div className="h-screen py-8 overflow-y-auto bg-white border-l border-r sm:w-64 w-60 dark:bg-gray-900 dark:border-gray-700 border-t">
        <div className="flex justify-between ">
          <h2 className="px-5 text-lg font-medium text-gray-800 dark:text-white">{decodeURI(name)}</h2>
          <NewNoteButton notePadName={name} />
        </div>

        <div className="mt-8 space-y-4">
          {noteButtons}
        </div>
      </div>
        {children}
        </NotePadContext.Provider>)
}
