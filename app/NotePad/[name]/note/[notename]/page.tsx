'use client'
import { useState, useEffect } from 'react'
import { usePathname, useRouter  } from 'next/navigation';
import {useNote} from '@/hooks/useNote'
export default function Page() {
  const pathname = usePathname()
  const splitPath = pathname.split('/')
  const notePadName = decodeURI(splitPath[2])
  const noteName = decodeURI(splitPath[4])
  const {note, fetchNote} = useNote({
    defaultNotePadName:notePadName,
    defaultNoteName: noteName
  })
  return (<div>
    { note && (
      <div className="max-w-md py-4 px-8 bg-white shadow-lg rounded-lg my-20">
        <div className="flex justify-center md:justify-end -mt-16">
          <img className="w-20 h-20 object-cover rounded-full border-2 border-indigo-500" src="https://images.unsplash.com/photo-1499714608240-22fc6ad53fb2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80"/>
        </div>
        <div>
          <h2 className="text-gray-800 text-3xl font-semibold">{note.name}</h2>
          <p className="mt-2 text-gray-600">{note.content}</p>
        </div>
        <div className="flex justify-end mt-4">
          <a href="#" className="text-xl font-medium text-indigo-500">{note.date}</a>
        </div>
      </div>
    )}
  </div>)
}
