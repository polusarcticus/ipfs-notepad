// NotePad/[name]/note/create/page.tsx

'use client'

import { useState, useContext, useEffect, ReactNode } from 'react'

import { usePathname, useRouter  } from 'next/navigation';

import axios from 'axios'
import { Toast } from '@/components/Toast'

import { NotePadContext } from '@/context/NotePadsContext'
export default function Page() {
  const {notes, fetchNotePad} = useContext(NotePadContext)
	const pathname = usePathname()
	const splitPath = pathname.split('/')
	const notePadName = decodeURI(splitPath[2])

	const [loadingNote, setLoadingNote] = useState(false);
	const [noteName, setNoteName] = useState("")
	const [note, setNote] = useState(null);
	const [txt, setTxt] = useState("");

	const [showToast, setShowToast] = useState(false)
	const [toastStatus, setToastStatus] = useState("")
	const [toastMessage, setToastMessage] = useState("")

	const handleNewNote = async () => {
		setLoadingNote(true)
		const res = await axios.post(`/api/notepad/${notePadName}/note`, {
			text: txt,
			name: noteName
		})
		console.log(res)
		if (res.data.success) {
			setToastMessage("Note Created")
			setToastStatus("success")
      fetchNotePad()
			setShowToast(true)
		} else {
			setToastMessage(res.data.error)
			setToastStatus("warning")
			setShowToast(true)
		}
		setLoadingNote(false)
	}
	const handleResetToast = () => {
		setShowToast(false)
	}
	return (
		<form className="w-full max-w-lgm m-4">
			<Toast trigger={showToast} status={toastStatus} message={toastMessage} handleResetToast={handleResetToast}/>
			<div className="text-2xl font-bold underline mb-3">Create Note</div>
			<div className="flex flex-wrap -mx-3 mb-6">
				<div className="w-full px-3">
					<label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="grid-password">
						Note Name
					</label>
					<input
						value={noteName}
						onChange={(event) => {setNoteName(event.target.value)}}
						className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"   type="text"/>
				</div>
			</div>
			<div className="flex flex-wrap -mx-3 mb-6">
				<div className="w-full px-3">
					<label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="grid-password">
						Content
					</label>
					<textarea onChange={(event) => setTxt(event.target.value)} value={txt} className=" no-resize appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 h-48 resize-none" id="message"></textarea>
				</div>
			</div>
			<div className="md:flex md:items-center">
				<div className="md:w-1/3">
					<button
						onClick={() => {handleNewNote()}}
						className={
							`shadow bg-teal-400 hover:bg-teal-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded ${loadingNote ? "animate-pulse" : ""}`
						}
						type="button">
						{loadingNote ? "Loading..." : "Create Note"}
					</button>
				</div>
				<div className="md:w-2/3"></div>
			</div>
		</form>

	)
}
