// NotePad/page.tsx
'use client'
import { useState, useContext, useEffect, ReactNode } from 'react'
import axios from 'axios'
import { Toast } from '@/components/Toast'
import {People, Pi, Bell, Gear} from '@/components/Logos'
import { NotePadsContext } from '@/context/NotePadsContext'
export default function Page() {
  const {notePads, fetchNotePads} = useContext(NotePadsContext)
  const [loadingNotePad, setLoadingNotePad] = useState(false);
  const [notePad, setNotePad] = useState(null)
  const [notePadName, setNotePadName] =useState("")
  const [logo, setLogo] = useState("people")
  const [showToast, setShowToast] = useState(false)
  const [toastStatus, setToastStatus] = useState("")
  const [toastMessage, setToastMessage] = useState("")
  const handleLogoSelect = (logoChoice:string) => {
    console.log('logo choice', logoChoice)
    setLogo(logoChoice)
  }

  const handleNewNotePad = async () => {
    setLoadingNotePad(true)
    const res = await axios.post("/api/notepad", {
      name: notePadName,
      logo: logo
    })
    console.log(res)
    if (res.data.success) {
      setToastMessage("Note Pad Created")
      setToastStatus("success")
      setShowToast(true)
    } else {
      setToastMessage(res.data.error)
      setToastStatus("warning")
      setShowToast(true)
    }
    fetchNotePads()
    setLoadingNotePad(false)
  }
  const handleResetToast = () => {
    setShowToast(false)
  }
  return (
    <form className="w-full max-w-lgm m-4">
    <Toast trigger={showToast} status={toastStatus} message={toastMessage} handleResetToast={handleResetToast}/>
      <div className="text-2xl font-bold underline mb-3">Create Notepad</div>
      <div className="flex flex-wrap -mx-3 mb-6">
        <div className="w-full px-3">
          <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="grid-password">
            Note Pad Name
          </label>
          <input
            value={notePadName}
            onChange={(event) => {setNotePadName(event.target.value)}}
          className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"   type="text"/>
        </div>
      </div>
      <div className="flex flex-wrap -mx-3 mb-6">
        <div className="w-full px-3">
          <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="logo">Select Logo</label>
          <div id="countries" className="flex flex-wrap appearance-none w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
            <LogoSelection name="people" Svg={<People />}
              logo={logo} handleLogoSelect={handleLogoSelect}/>
            <LogoSelection name="pi" Svg={<Pi />}
              logo={logo} handleLogoSelect={handleLogoSelect}/>
            <LogoSelection name="bell" Svg={<Bell />}
              logo={logo} handleLogoSelect={handleLogoSelect}/>
            <LogoSelection name="gear" Svg={<Gear />}
              logo={logo} handleLogoSelect={handleLogoSelect}/>
          </div>
        </div>
      </div>
      <div className="md:flex md:items-center">
        <div className="md:w-1/3">
          <button
            onClick={() => {handleNewNotePad()}}
            className={
              `shadow bg-teal-400 hover:bg-teal-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded ${loadingNotePad ? "animate-pulse" : ""}`
            }
            type="button">
              {loadingNotePad ? "Loading..." : "Create Notepad"}
          </button>
        </div>
        <div className="md:w-2/3"></div>
      </div>
    </form>
  )
}

const LogoSelection = ({
  logo,
  handleLogoSelect,
  name,
  Svg
}: {
  logo: string,
  handleLogoSelect: Function,
  name: string,
  Svg: JSX.Element
}) => {
  const [selected, setSelected] = useState(false)
  useEffect(() => {
    console.log('logo', logo)
    if (logo === name) {
      setSelected(true)
    } else {
      setSelected(false)
    }
  }, [logo, name])
  return (
    <a
      onClick={() => {
        handleLogoSelect(name)}}
      className={
        `mx-2 hover:bg-gray-100 rounded-lg p-1.5
        ${selected ? 'bg-blue-300' : ''}
      `}
    >
      {Svg}
    </a>
  )
}

