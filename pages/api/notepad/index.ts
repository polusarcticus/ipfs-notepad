// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import { fromString   } from 'uint8arrays/from-string'
import { toString   } from 'uint8arrays/to-string'
import { concat } from 'uint8arrays/concat'
import { MFSEntry } from 'ipfs-core-types'

import all from 'it-all'

import {client} from '../ipfs'
// arbitrary response format
export type NotePadData = {
  cid: string;
  content: string;
  notes: Array<string| MFSEntry>;
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method === "POST") {
    // Process a POST request
    addNotePad(req, res)
  } else {
    // Handle any other HTTP method
    retrieveNotePads(req, res);
  }
}

const addNotePad = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  const name = req.body.name
  const logo = req.body.logo
  const dir = `/notepad/${name}`
  try {
    // check for existence
    const stat = await client.files.stat(dir)
    console.log('stat', stat)
    res.status(200).json({success: false, error: 'A notepad with this name has already been created'})
    return 
  } catch (e) {
    console.log(e)
    console.log('does not yet exist, creating')
  }

  await client.files.mkdir(dir, {parents: true})
  await client.files.write(
    `/notepad/${name}.json`,
    Buffer.from(JSON.stringify({
        created: new Date(),
        logo: logo,
      }),'utf8'),
    {create: true}
  )
  const stat = await client.files.stat(dir)
  res.status(200).json({success: true, stat})
}

const retrieveNotePads = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  // connect to the default API address http://localhost:5001
  const dir = `/notepad`
  console.log(dir)
  const notePadsData = []
  for await (const file of client.files.ls(dir)) {
    if (file.type === 'file') {
      console.log(file)
      const padData = JSON.parse(toString(concat(await all(client.files.read(`${dir}/${file.name}`)))))
      padData.name = file.name.slice(0, -5)
      notePadsData.push(padData)
    }
  }
  console.log('notepadsdata', notePadsData)
  res.status(200).json({notePadsData });
  // call Core API methods

};
