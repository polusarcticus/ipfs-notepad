
// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import { fromString   } from 'uint8arrays/from-string'
import { toString   } from 'uint8arrays/to-string'
import { concat } from 'uint8arrays/concat'

import all from 'it-all'
import {client, BasicIpfsData} from '@/pages/api/ipfs'
// arbitrary response format

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<BasicIpfsData[] | BasicIpfsData>
) {
  if (req.method === "POST") {
    // Process a POST request
  } else {
    // Handle any other HTTP method
    retrieveNotePad(req, res);
  }
}
const retrieveNotePad = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  const dir = `/notepad/${req.query.notepadid}`
  console.log('dir', dir)
  const ls = await all(client.files.ls(dir))
  const notes = await Promise.all(ls.map(async (d,i) => {
    const note = JSON.parse(toString(concat(await all(client.files.read(
      `${dir}/${d.name}`
    )))))
    note['name'] = d.name
    return note
  }))
  // connect to the default API address http://localhost:5001
  // call Core API methods
  /*
  for await (const { cid, type  } of client.pin.ls()) {
    console.log('cid', cid)
    const note = toString(concat(await all(client.get(cid))))
    console.log('note', note)
    notes.push({cid: cid, content: note.content })
  }
  console.log(pins);
 */
  res.status(200).json({notes});
};
