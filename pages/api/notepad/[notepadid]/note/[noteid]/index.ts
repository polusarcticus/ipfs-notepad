// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import { fromString   } from 'uint8arrays/from-string'
import { toString   } from 'uint8arrays/to-string'
import { concat } from 'uint8arrays/concat'

import all from 'it-all'

import {client, BasicIpfsData} from '@/pages/api/ipfs'
// arbitrary response format

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method === "POST") {
    // Process a POST request
  } else {
    // Handle any other HTTP method
    retrieveNote(req, res);
  }
}
const retrieveNote = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  // connect to the default API address http://localhost:5001
  // call Core API methods
  console.log('query params',req.query.notepadid, req.query.noteid)
  const note  = JSON.parse(toString(concat(await all(client.files.read(
    `/notepad/${req.query.notepadid}/${decodeURI(req.query.noteid)}`
  )))))
  note.name = req.query.noteid
  res.status(200).json({note });
};
