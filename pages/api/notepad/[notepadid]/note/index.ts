// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import { fromString   } from 'uint8arrays/from-string'
import { toString   } from 'uint8arrays/to-string'
import { concat } from 'uint8arrays/concat'

import all from 'it-all'
import {client, BasicIpfsData} from '@/pages/api/ipfs'
// arbitrary response format

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<BasicIpfsData[] | BasicIpfsData>
) {
  if (req.method === "POST") {
    // Process a POST request
    addNote(req, res)
  } else {
    // Handle any other HTTP method
    retrieveNotes(req, res);
  }
}
const addNote = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  const dir =  `/notepad/${req.query.notepadid}`
  await client.files.write(
    `${dir}/${req.body.name}`,
    Buffer.from(JSON.stringify({
      date: new Date(),
      content: req.body.text
}), 'utf8'),
    {create: true}
  )
  const ls = await all(client.files.ls(dir))
  //const pin = await client.pin.add(data.cid)
  const read = toString(concat(await all(client.files.read(`${dir}/${req.body.name}`))))
  console.log(read)
  res.status(200).json({success: true, note:read})
}



const retrieveNotes = async (
  req: NextApiRequest,
  res: NextApiResponse<BasicIpfsData[]>
) => {
  // connect to the default API address http://localhost:5001
  console.log('reqbody', req.body)
  // call Core API methods
  const string = "Hello world!";
  const pins = await client.pin.ls()
  console.log('pins', pins)
  const notes: BasicIpfsData[] = []
  for await (const { cid, type  } of client.pin.ls()) {
    console.log('cid', cid)
    const note = toString(concat(await all(client.get(cid))))
    console.log('note', note)
    notes.push({cid: cid, content: note.content })
  }
  console.log(pins);

  res.status(200).json({notes });
};
