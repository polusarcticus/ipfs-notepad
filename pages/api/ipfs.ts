import { create } from "ipfs-http-client";
export const client = create();
export type BasicIpfsData = {
  cid: string;
  content: string;
};
