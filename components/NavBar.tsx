import Link from 'next/link'

export const NavBar = () => {
	return (
		<div className="shadow bg-white">
			<div className="h-16 mx-auto px-5 flex items-center justify-between">
				<Link href="/" className="text-2xl hover:text-cyan-500 transition-colors cursor-pointer">IPFS Notes</Link>

				<ul className="flex items-center gap-5">
					<li><a className="hover:text-cyan-500 transition-colors" href="">DeSci</a></li>
					<li><a className="hover:text-cyan-500 transition-colors" href="">Gitlab</a></li>
					<li><a className="hover:text-cyan-500 transition-colors" href="">Choose Network</a></li>
					<li><a className="hover:text-cyan-500 transition-colors" href="">Connect Wallet</a></li>
				</ul>
			</div>
		</div>
	)
}
