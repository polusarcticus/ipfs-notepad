'use client';
import { ReactNode, useState, useEffect } from 'react'
import { NewNotePadButton } from './NewNotePadButton'
import {useNotePads} from '@/hooks/useNotePads'
import {logoMap} from '@/components/Logos'

import {NotePadsContext} from '@/context/NotePadsContext'
export const SideBar = ({
  children
}:{
  children: ReactNode
}) => {
  const {notePads, fetchNotePads} = useNotePads()
  const [notePadButtons, setNotePadButtons] = useState([])

  useEffect(() => {
    //@ts-ignore
    setNotePadButtons(notePads.map((pad, i) => {
      return (
        <a key={i} href={`/NotePad/${pad.name}`} className="p-1.5 text-gray-500 focus:outline-nones transition-colors duration-200 rounded-lg dark:text-gray-400 dark:hover:bg-gray-800 hover:bg-gray-100">
          {logoMap[pad.logo]}
        </a>
      )
    }))

  }, [notePads])
  
  return (
    <NotePadsContext.Provider value={{notePads, fetchNotePads}}>
    <aside className="flex">
      <div className="flex flex-col items-center w-16 h-screen py-8 space-y-8 bg-white dark:bg-gray-900 dark:border-gray-700 border-t">
        {notePadButtons}
        <NewNotePadButton />
      </div>

      {children}
    </aside>
    </NotePadsContext.Provider>
  )

}

