import { useState, useEffect, useCallback } from 'react'
import axios from 'axios'

export const useNotePads = () => {
  const [notePads, setNotePads] = useState([])

  const fetchNotePads = useCallback(async () => {
    try {
      const { data } = await axios.get("/api/notepad");
      setNotePads(data.notePadsData)
    } catch (e) {
      console.log(e)
    }
  }, [])

  useEffect(() => {
    if (!notePads.length) {
      fetchNotePads()
    }
  }, [])
  return {
    notePads,
    fetchNotePads,
  }
}


export const useNotePad = ({defaultName=null} = {}) => {
  const [notes, setNotes] = useState([])
  const fetchNotePad = useCallback(async ({name=defaultName}= {}) => {
    console.log('defaultName', defaultName)
    const res = await axios.get(`/api/notepad/${name}`)
    console.log('res', res)
    setNotes(res.data.notes)
  }, [])

  useEffect(() => {
    if (defaultName) {
      fetchNotePad()
    }
  }, [])
  return { notes, fetchNotePad }
}
