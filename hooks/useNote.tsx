'use client'
import {useState, useEffect, useCallback} from 'react'
import axios from 'axios'
export const useNote = ({defaultNotePadName=null, defaultNoteName=null} = {}) => {
  const [note, setNote] =useState({})
  const fetchNote = useCallback(async ({notePadName=defaultNotePadName, noteName=defaultNoteName} = {}) => {
    const  res = await axios.get(`/api/notepad/${notePadName}/note/${noteName}`)
    console.log(res)
    setNote(res.data.note)
  }, [])

  useEffect(() => {
    if (defaultNotePadName && defaultNoteName) {
      fetchNote()
    }
  }, [])
  return {note, fetchNote}
}
